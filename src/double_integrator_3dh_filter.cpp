/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * double_integrator_3dh_filter.cpp
 *
 *  Created on: Mar 5, 2016
 *      Author: Sanjiban Choudhury
 */

#include "dynamics_filter/double_integrator_3dh_filter.h"
#include "math_utils/math_utils.h"
#include "polynomials/polynomial_fit.h"
#include <angles/angles.h>
#include <iostream>

namespace vm = ca::math_utils::vector_math;
namespace pf = ca::polynomial_fit;
namespace no = ca::math_utils::numeric_operations;

namespace ca {

bool DoubleIntegrator3DHFilter::Filter(const Trajectory &infeasible_trajectory, const State &start, Trajectory &feasible_trajectory, double dt) const {
  unsigned int traj_idx = 0, new_idx = 0;
  feasible_trajectory = Trajectory(1, start);
  State current_state = start;
  while (traj_idx < infeasible_trajectory.size() - 1) {
    double reference_distance = 0;
    bool result = LookaheadIndex(current_state, infeasible_trajectory, traj_idx, new_idx);
    if (!result)
      return false;
    traj_idx = new_idx;
    DynamicsStep(current_state, infeasible_trajectory, traj_idx, dt, current_state);
    feasible_trajectory.push_back(current_state);
  }
  StitchToEnd(infeasible_trajectory.back(), feasible_trajectory, dt, feasible_trajectory);
  return true;
}

void DoubleIntegrator3DHFilter::DynamicsStep(const State &state, const Trajectory &traj, unsigned int traj_idx, double dt, State &new_state) const {
  State ref_state = traj[traj_idx];
  Eigen::Vector2d dir = vm::SafeNormalize( Eigen::Vector2d(ref_state.vel.x(),ref_state.vel.y()));
  double ed = (state.pos.x() - ref_state.pos.x())*(-dir.y()) + (state.pos.y() - ref_state.pos.y())*dir.x();

  Eigen::Vector3d acc;
  acc[0] = - params_.vel_gain*(state.vel.x() - ref_state.vel.x()) + params_.pos_gain*ed*dir.y();
  acc[1] = - params_.vel_gain*(state.vel.y() - ref_state.vel.y()) - params_.pos_gain*ed*dir.x();
  acc[2] = - params_.velz_gain*(state.vel.z() - ref_state.vel.z()) - params_.posz_gain*(state.pos.z() - ref_state.pos.z());
  acc = vm::Limit(Eigen::Vector3d(limits_.max_acc, limits_.max_acc, limits_.max_acc), acc);

  double del_psi = angles::normalize_angle(ref_state.psi - state.psi);
  double psid = params_.psi_gain*del_psi;
  no::Limit(limits_.max_psid, psid);

  Eigen::Vector3d vel = state.vel + acc*dt;
  vel = vm::Limit(Eigen::Vector3d(limits_.max_vel, limits_.max_vel, limits_.max_vel), vel);

  Eigen::Vector3d pos = state.pos + 0.5*dt*(vel + state.vel);
  double psi_new = state.psi + dt*psid;

  new_state.t = state.t + dt;
  new_state.pos = pos;
  new_state.psi = psi_new;
  new_state.vel = vel;
  new_state.psid = psid;
  new_state.acc = acc;
}

bool DoubleIntegrator3DHFilter::LookaheadIndex(const State &state, const Trajectory &traj, unsigned int start_idx, unsigned int &new_idx) const {
  new_idx = start_idx;
  double lookahead_dist = LookAheadDist(state);
  if (traj.size() >= 2) {
    Trajectory::const_iterator it, it_end = traj.end();
    it = traj.begin()+1;
    if(new_idx >= 1 && new_idx < (int)traj.size())
      it = traj.begin() + new_idx;
    for(;it!=it_end; it++) {
      Eigen::Vector3d v1 = (*(it  )).pos;
      Eigen::Vector3d v2 = (*(it-1)).pos;

      if((v1 - v2).norm() < 1e-3)
        continue;

      double dists = vm::DistanceSidePlane(v1, v2, state.pos)- lookahead_dist;
      //TODO - inserted line below
      double eucl = (state.pos - v1).norm();
      if(dists>0 && eucl < params_.tracking_tolerance) {
        new_idx = it - traj.begin();
        return true;
      }
    }

    if(it >= it_end)
      it = it_end-1;
    new_idx = it - traj.begin();
    double eucl = (state.pos - it->pos).norm();
    if(eucl < params_.tracking_tolerance)
      return true;
  }
  return false;
}


void DoubleIntegrator3DHFilter::StitchToEnd(const State &final, const Trajectory &feasible_trajectory, double dt, Trajectory &stitched_trajectory) const {
  stitched_trajectory = feasible_trajectory;

  if (params_.stitching_distance <= 0 || final.vel.norm() > 0.5)
    return;

  Trajectory::reverse_iterator rit = stitched_trajectory.rbegin();
  for (; rit!= stitched_trajectory.rend(); ++rit)
    if ((rit->pos - final.pos).norm() > params_.stitching_distance)
      break;

  State stitch;
  if (rit == stitched_trajectory.rend())
    stitch = stitched_trajectory.front();
  else
    stitch = *rit;

  stitched_trajectory.erase(rit.base(), stitched_trajectory.end());
  if (stitched_trajectory.size() == 0)
    stitched_trajectory.push_back(stitch);

  Polynomial<double, 3> ppx = pf::GetFeasInterpCubicPoly(Eigen::Vector2d(stitch.pos.x(), final.pos.x()),
                                                         Eigen::Vector2d(stitch.vel.x(), final.vel.x()),
                                                         limits_.max_vel, limits_.max_acc);

  Polynomial<double, 3> ppy = pf::GetFeasInterpCubicPoly(Eigen::Vector2d(stitch.pos.y(), final.pos.y()),
                                                         Eigen::Vector2d(stitch.vel.y(), final.vel.y()),
                                                         limits_.max_vel, limits_.max_acc);

  Polynomial<double, 3> ppz = pf::GetFeasInterpCubicPoly(Eigen::Vector2d(stitch.pos.z(), final.pos.z()),
                                                         Eigen::Vector2d(stitch.vel.z(), final.vel.z()),
                                                         limits_.max_vel, limits_.max_acc);

  double del_psi = angles::normalize_angle(stitch.psi - final.psi);
  Polynomial<double, 1> pph = pf::GetFeasInterpLinearPoly(Eigen::Vector2d(stitch.psi, stitch.psi + del_psi),
                                                          limits_.max_psid);

  std::vector<double> t_set = {ppx.break_u(), ppy.break_u(), ppz.break_u(), pph.break_u()};
  double final_t = *std::max_element(t_set.begin(), t_set.end());

  ppx = pf::GetCubicPoly(stitch.t, stitch.t + final_t,
                         Eigen::Vector2d(stitch.pos.x(), final.pos.x()),
                         Eigen::Vector2d(stitch.vel.x(), final.vel.x()));

  ppy = pf::GetCubicPoly(stitch.t, stitch.t + final_t,
                         Eigen::Vector2d(stitch.pos.y(), final.pos.y()),
                         Eigen::Vector2d(stitch.vel.y(), final.vel.y()));

  ppz = pf::GetCubicPoly(stitch.t, stitch.t + final_t,
                         Eigen::Vector2d(stitch.pos.z(), final.pos.z()),
                         Eigen::Vector2d(stitch.vel.z(), final.vel.z()));

  pph = pf::GetLinearPoly(stitch.t, stitch.t + final_t,  Eigen::Vector2d(stitch.psi, stitch.psi + del_psi));

  Polynomial<double, 2> ppxd = ppx.Derivative();
  Polynomial<double, 1> ppxdd = ppxd.Derivative();
  Polynomial<double, 2> ppyd = ppy.Derivative();
  Polynomial<double, 1> ppydd = ppyd.Derivative();
  Polynomial<double, 2> ppzd = ppz.Derivative();
  Polynomial<double, 1> ppzdd = ppzd.Derivative();
  Polynomial<double, 0> pphd = pph.Derivative();

  int num_idx = std::max(2, (int)std::round(final_t / dt));
  for (int i = 1; i < num_idx; i++) {
    double t = stitch.t + ((double)i / (num_idx - 1))*final_t;
    State st;
    st.t = t;
    st.pos = Eigen::Vector3d(ppx.Evaluate(t), ppy.Evaluate(t), ppz.Evaluate(t));
    st.psi = pph.Evaluate(t);
    st.vel = Eigen::Vector3d(ppxd.Evaluate(t), ppyd.Evaluate(t), ppzd.Evaluate(t));
    st.psid = pphd.Evaluate(t);
    st.acc = Eigen::Vector3d(ppxdd.Evaluate(t), ppydd.Evaluate(t), ppzdd.Evaluate(t));
    stitched_trajectory.push_back(st);
  }
}

double DoubleIntegrator3DHFilter::LookAheadDist(const State &state) const {
  double vel = state.vel.norm();
  double alpha = std::max( 0.0, std::min( 1.0, (vel - params_.lookahead_vel_lb) / (params_.lookahead_vel_ub - params_.lookahead_vel_lb) ) );
  return (1 - alpha)*params_.lookahead_dist_lb + alpha*params_.lookahead_dist_ub;
}


}


