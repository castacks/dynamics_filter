/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * double_integrator_2d_filter.cpp
 *
 *  Created on: Mar 2, 2016
 *      Author: Sanjiban Choudhury
 */

#include "dynamics_filter/double_integrator_2d_filter.h"
#include "math_utils/math_utils.h"
#include "polynomials/polynomial_fit.h"

//#include <iostream>
namespace vm = ca::math_utils::vector_math;
namespace pf = ca::polynomial_fit;
namespace ca {

void DoubleIntegrator2DFilter::Filter(const Trajectory &infeasible_trajectory, const State &start, Trajectory &feasible_trajectory, double dt) const {
  unsigned int traj_idx = 0;
  feasible_trajectory = Trajectory(1, start);
  State current_state = start;
  while (traj_idx < infeasible_trajectory.size() - 1) {
    traj_idx = LookaheadIndex(current_state, infeasible_trajectory, traj_idx);
    DynamicsStep(current_state, infeasible_trajectory, traj_idx, dt, current_state);
    //std::cout << traj_idx << " " << current_state.pos.transpose() << " " << current_state.vel.transpose() << " " << current_state.acc.transpose() << std::endl;
    feasible_trajectory.push_back(current_state);
  }

  StitchToEnd(infeasible_trajectory.back(), feasible_trajectory, dt, feasible_trajectory);
}

void DoubleIntegrator2DFilter::DynamicsStep(const State &state, const Trajectory &traj, unsigned int traj_idx, double dt, State &new_state) const {
  State ref_state = traj[traj_idx];
  Eigen::Vector2d dir = vm::SafeNormalize(ref_state.vel);
  double ed = (state.pos.x() - ref_state.pos.x())*(-dir.y()) + (state.pos.y() - ref_state.pos.y())*dir.x();

  Eigen::Vector2d acc;
  acc[0] = - params_.vel_gain*(state.vel.x() - ref_state.vel.x()) + params_.pos_gain*ed*dir.y();
  acc[1] = - params_.vel_gain*(state.vel.y() - ref_state.vel.y()) - params_.pos_gain*ed*dir.x();
  acc = vm::Limit(Eigen::Vector2d(limits_.max_acc, limits_.max_acc), acc);

  Eigen::Vector2d vel = state.vel + acc*dt;
  vel = vm::Limit(Eigen::Vector2d(limits_.max_vel, limits_.max_vel), vel);

  Eigen::Vector2d pos = state.pos + 0.5*dt*(vel + state.vel);

  new_state.t = state.t + dt;
  new_state.pos = pos;
  new_state.vel = vel;
  new_state.acc = acc;
}

unsigned int DoubleIntegrator2DFilter::LookaheadIndex(const State &state, const Trajectory &traj, unsigned int start_idx) const {
  unsigned int new_idx = start_idx;
  if (traj.size() >= 2) {
    Trajectory::const_iterator it, it_end = traj.end();
    it = traj.begin()+1;
    if(new_idx >= 1 && new_idx < (int)traj.size())
      it = traj.begin() + new_idx;
    for(;it!=it_end; it++) {
      Eigen::Vector2d v1 = (*(it  )).pos;
      Eigen::Vector2d v2 = (*(it-1)).pos;

      if((v1 - v2).norm() < 1e-3)
        continue;

      double dists = vm::DistanceSidePlane(v1, v2, state.pos)- params_.lookahead_dist;
      //TODO - inserted line below
      double eucl = (state.pos - v1).norm();
      if(dists>0 && eucl < 3*params_.lookahead_dist)
      {
        new_idx = it - traj.begin();
        break;
      }
    }

    if(it >= it_end)
      it = it_end-1;
  }
  return new_idx;
}


void DoubleIntegrator2DFilter::StitchToEnd(const State &final, const Trajectory &feasible_trajectory, double dt, Trajectory &stitched_trajectory) const {
  stitched_trajectory = feasible_trajectory;

  Trajectory::reverse_iterator rit = stitched_trajectory.rbegin();
  for (; rit!= stitched_trajectory.rend(); ++rit)
    if ((rit->pos - final.pos).norm() > params_.stitching_distance)
      break;

  State stitch;
  if (rit == stitched_trajectory.rend())
    stitch = stitched_trajectory.front();
  else
    stitch = *rit;

  stitched_trajectory.erase(rit.base(), stitched_trajectory.end());

  Polynomial<double, 3> ppx = pf::GetFeasInterpCubicPoly(Eigen::Vector2d(stitch.pos.x(), final.pos.x()),
                                                         Eigen::Vector2d(stitch.vel.x(), final.vel.x()),
                                                         limits_.max_vel, limits_.max_acc);

  Polynomial<double, 3> ppy = pf::GetFeasInterpCubicPoly(Eigen::Vector2d(stitch.pos.y(), final.pos.y()),
                                                         Eigen::Vector2d(stitch.vel.y(), final.vel.y()),
                                                         limits_.max_vel, limits_.max_acc);

  double final_t = std::max(ppx.break_u(), ppy.break_u());

  ppx = pf::GetCubicPoly(stitch.t, stitch.t + final_t,
                                               Eigen::Vector2d(stitch.pos.x(), final.pos.x()),
                                               Eigen::Vector2d(stitch.vel.x(), final.vel.x()));

  ppy = pf::GetCubicPoly(stitch.t, stitch.t + final_t,
                                               Eigen::Vector2d(stitch.pos.y(), final.pos.y()),
                                               Eigen::Vector2d(stitch.vel.y(), final.vel.y()));

  Polynomial<double, 2> ppxd = ppx.Derivative();
  Polynomial<double, 1> ppxdd = ppxd.Derivative();
  Polynomial<double, 2> ppyd = ppy.Derivative();
  Polynomial<double, 1> ppydd = ppyd.Derivative();

  int num_idx = std::max(2, (int)std::round(final_t / dt));
  for (int i = 1; i < num_idx; i++) {
    double t = stitch.t + ((double)i / (num_idx - 1))*final_t;
    State st;
    st.t = t;
    st.pos = Eigen::Vector2d(ppx.Evaluate(t), ppy.Evaluate(t));
    st.vel = Eigen::Vector2d(ppxd.Evaluate(t), ppyd.Evaluate(t));
    st.acc = Eigen::Vector2d(ppxdd.Evaluate(t), ppydd.Evaluate(t));
    stitched_trajectory.push_back(st);
  }
}


}
