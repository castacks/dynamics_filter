/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * double_integrator_2d_filter.h
 *
 *  Created on: Mar 2, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef DYNAMICS_FILTER_INCLUDE_DYNAMICS_FILTER_DOUBLE_INTEGRATOR_2D_FILTER_H_
#define DYNAMICS_FILTER_INCLUDE_DYNAMICS_FILTER_DOUBLE_INTEGRATOR_2D_FILTER_H_

#include <Eigen/Dense>
#include <vector>

namespace ca {

class DoubleIntegrator2DFilter {
public:
  struct State {
    double t;
    Eigen::Vector2d pos;
    Eigen::Vector2d vel;
    Eigen::Vector2d acc;
  };

  typedef std::vector<State> Trajectory;

  struct Limits {
    double max_vel, max_acc;
  };

  struct Params {
    double vel_gain, pos_gain, lookahead_dist; //for quadrotor 1, 0.5, 2 works
    double stitching_distance;
  };

  DoubleIntegrator2DFilter()
  : limits_(), params_() {}

  ~DoubleIntegrator2DFilter() {}

  void Filter(const Trajectory &infeasible_trajectory, const State &start, Trajectory &feasible_trajectory, double dt) const;

  void set_limits(const Limits &limits) {limits_ = limits;}
  void set_params(const Params &params) {params_ = params;}

  const Limits& limits() {return limits_;}
  const Params& params() {return params_;}
private:

  void DynamicsStep(const State &state, const Trajectory &traj, unsigned int traj_idx, double dt, State &new_state) const;

  unsigned int LookaheadIndex(const State &state, const Trajectory &traj, unsigned int start_idx) const;

  void StitchToEnd(const State &final, const Trajectory &feasible_trajectory, double dt, Trajectory &stitched_trajectory) const;

  Limits limits_;
  Params params_;
};

}




#endif /* DYNAMICS_FILTER_INCLUDE_DYNAMICS_FILTER_DOUBLE_INTEGRATOR_2D_FILTER_H_ */
